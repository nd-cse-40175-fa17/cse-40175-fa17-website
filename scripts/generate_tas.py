#!/usr/bin/env python2

import random
import yaml


graders = []
for ta in yaml.load(open('static/yaml/ta.yaml')):
    graders.append({
        'netid': ta['netid'],
        'blogs': [],
    })

blogs = []

for blog in yaml.load(open('static/yaml/blogs.yaml')):
    blogs.append({
        'netid':    blog['netid'].lower(),
        'url':      blog['url'],
    })

random.shuffle(blogs)

grader = 0
while blogs:
    blog = blogs.pop()
    graders[grader]['blogs'].append(blog)
    grader = (grader + 1) % len(graders)

print yaml.dump(graders, default_flow_style=False)
