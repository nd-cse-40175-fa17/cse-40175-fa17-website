#!/bin/sh

export PATH=/opt/anaconda-4.1.1/bin:$PATH

XML_PATH=/net/smb/pbui@fs.nd.edu/www/teaching/cse.40175.fa17/rss.xml
YML_PATH=$(dirname $0)/../static/yaml/blogs.yaml

# Update blogs yaml from Googe Spreadsheet
if (cd $(dirname $0) ; ./sheet_to_yaml.py) > ${YML_PATH}.tmp; then
    cp -f ${YML_PATH}.tmp ${YML_PATH}
    rm -f ${YML_PATH}.tmp
fi

# Update planet
if (cd $(dirname $0) ; ./planet.py) > ${XML_PATH}.tmp; then
    cp -f ${XML_PATH}.tmp ${XML_PATH}
    rm -f ${XML_PATH}.tmp
fi
