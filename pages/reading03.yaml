title:      "Reading 03: Immigration, Work-Life Balance"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week focus on issues related to the workplace:
    competition from immigrants and the idea of a work-life balance.

  ### Immigration

  These readings provide some background on the [H-1B] visa and lists what
  companies are utilizing this immigration program.

  - [Understanding H-1B Requirements](https://www.uscis.gov/eir/visa-guide/h-1b-specialty-occupation/understanding-h-1b-requirements)

  - [Top 100 H1B Visa Sponsors](http://www.myvisajobs.com/Reports/2016-H1B-Visa-Sponsor.aspx)

  - [H1B Visa Salary Database](http://h1bdata.info/)

  These readings discuss some of the controversy regarding the use of [H-1B]
  visas.

  - [America's Mixed Feelings About Immigrant Labor: Disney-Layoffs
    Edition](http://www.theatlantic.com/business/archive/2015/06/disney-h1b-visas-immigration-layoffs/396149/)

  - [Ex-Disney IT workers sue after being asked to train their own H-1B
    replacements](http://arstechnica.com/tech-policy/2016/01/ex-disney-it-workers-sue-after-being-forced-to-train-their-own-h-1b-replacements/)

  - [Is the H-1B Program a Cynical Attempt to Undercut American Workers?](https://www.theatlantic.com/business/archive/2017/02/the-dark-side-of-the-h-1b-program/516813/)

  - [Not Everyone in Tech Cheers Visa Program for Foreign Workers](https://www.nytimes.com/2017/02/05/business/h-1b-visa-tech-cheers-for-foreign-workers.html?_r=0)

  These readings argue the pros and cons of immigration as it relates to the
  technology industry, including an examination of if there is a tech-talent
  shortage.

  - [There Is In Fact A Tech-Talent Shortage And There Always Will Be](http://techcrunch.com/2013/05/05/there-is-in-fact-a-tech-talent-shortage-and-there-always-will-be/)

  - [Study: Immigrants Founded 51% of U.S. Billion-Dollar Startups](http://blogs.wsj.com/digits/2016/03/17/study-immigrants-founded-51-of-u-s-billion-dollar-startups/)

  - [The Bogus High-Tech Worker Shortage: How Guest Workers Lower US Wages](http://www.pbs.org/newshour/making-sense/the-bogus-high-tech-worker-sho/)

  - [New data on H-1B visas prove that IT outsourcers hire a lot but pay very little](https://qz.com/1041506/new-data-on-h-1b-visas-show-how-it-outsourcers-are-short-changing-workers/)

  These readings further discuss the need for reforming the [H-1B] visa program.

  - [Don't Give Silicon Valley More H1B Visas](http://www.realclearpolitics.com/articles/2017/01/14/dont_give_silicon_valley_more_h1b_visas_132795.html)

  - [Increasingly, U.S. IT workers are alleging
    discrimination](http://www.networkworld.com/article/2988324/careers/increasingly-u-s-it-workers-are-alleging-discrimination.html)

  - [Silicon Valley's "Body Shop" Secret: Highly Educated Foreign Workers Treated Like Indentured Servants](http://www.nbcbayarea.com/investigations/Silicon-Valleys-Body-Shop-Secret-280567322.html)

  - [Commentary: The H-1B Visa Problem as IEEE-USA Sees It](https://spectrum.ieee.org/view-from-the-valley/at-work/tech-careers/commentary-the-h1b-problem-as-ieeeusa-sees-it)

      Also: [Four Ways to Tackle H-1B Visa Reform](https://spectrum.ieee.org/tech-talk/at-work/tech-careers/four-ways-to-tackle-h1b-visa-reform)

  These readings discuss the Trump administration's plans for the [H-1B] visa program:

  - [Trump Cracks Down on H-1B Visa Program That Feeds Silicon Valley](https://www.bloomberg.com/news/articles/2017-04-03/new-h-1b-guidelines-crack-down-on-computer-programmer-jobs)

  - [Trump will sign executive order to begin revamp of H-1B visa program](https://arstechnica.com/tech-policy/2017/04/trump-will-sign-executive-order-to-begin-revamp-of-h-1b-visa-program/)

  - [Disdainful of H-1Bs, Trump expands a different foreign worker visa](https://arstechnica.com/tech-policy/2017/07/disdainful-of-h-1bs-trump-expands-a-different-foreign-worker-visa/)

  These readings discuss some reactions to the Trump administration's plans for the [H-1B] visa program:

  - [Lets talk about the H1-B Visa](https://medium.com/@fwiwm2c/lets-talk-about-the-h1-b-visa-6e5d5def2b00)

  - [How Immigration Uncertainty Threatens America’s Tech Dominance](http://archive.is/BHfyN)

  - [Indian IT firms have been preparing for changes in H-1B visa laws for nearly a decade](https://qz.com/901292/indian-it-firms-like-wipro-tcs-and-infosys-have-been-preparing-for-changes-in-h1b-visa-laws-and-donald-trumps-america-for-several-years/?ICID=ref_fark)

  Finally, these articles discuss the recent Trump administration announcement regarding the end of [DACA]:

  - [Admin memo: DACA recipients should prepare for 'departure from the United States'](http://www.cnn.com/2017/09/05/politics/white-house-memo-daca-recipients-leave/index.html)

  - [Urgent DACA legislation is both an economic imperative and humanitarian necessity](https://blogs.microsoft.com/on-the-issues/2017/09/05/urgent-daca-legislation-economic-imperative-humanitarian-necessity/)

  - [From Apple to Y Combinator—tech sector denounces new “Dreamers”
    plan](https://arstechnica.com/tech-policy/2017/09/from-apple-to-y-combinator-tech-denounces-new-dreamers-immigration-plan/)

  [H-1B]: https://en.wikipedia.org/wiki/H-1B_visa
  [DACA]: https://en.wikipedia.org/wiki/Deferred_Action_for_Childhood_Arrivals

  ### Work-Life Balance

  These articles discuss the difficulty of balancing work and family life.

  - [Why Women Still Can't Have It
    All](http://www.theatlantic.com/magazine/archive/2012/07/why-women-still-cant-have-it-all/309020/)

  - [Why I Put My Wife's Career First](http://www.theatlantic.com/magazine/archive/2015/10/why-i-put-my-wifes-career-first/403240/)

  - [The Work-Family Imbalance](http://techcrunch.com/2015/04/04/the-work-family-imbalance/)

  - [The Revolt of Working
    Parents](https://www.theatlantic.com/business/archive/2017/01/the-new-glass-ceiling/512834/)

  These articles discuss the pressures to make work a priority in the tech industry and in America in general:

  - [In Silicon Valley, Working 9 to 5 Is for Losers](https://www.nytimes.com/2017/08/31/opinion/sunday/silicon-valley-work-life-balance-.html)

  - [I Came to San Francisco to Change My Life: I Found a Tribe of Depressed Workaholics Living on Top of One Another](http://www.alternet.org/labor/hacker-house-blues-my-life-12-programmers-2-rooms-and-one-21st-century-dream)

  - [Americans Work 25% More Than Europeans, Study Finds](https://www.bloomberg.com/news/articles/2016-10-18/americans-work-25-more-than-europeans-study-finds)

  These articles discuss the struggling with this balance in the tech industry:

  - [Silicon Valley: Perks for Some Workers, Struggles for Parents](http://www.nytimes.com/2015/04/08/upshot/silicon-valley-perks-for-some-workers-struggles-for-parents.html)

  - [Why Tech Is the Leading Industry on Parental Leave](https://www.theatlantic.com/business/archive/2016/03/tech-paid-paternity-leave/473922/)

  - [Silicon Valley's Best and Worst Jobs for New Moms (and Dads)](http://www.theatlantic.com/technology/archive/2015/03/the-best-and-worst-companies-for-new-moms-and-dads-in-silicon-valley/386384/)

  These articles focus on this issue at [Amazon]:

  - [Inside Amazon: Wrestling Big Ideas in a Bruising Workplace](http://www.nytimes.com/2015/08/16/technology/inside-amazon-wrestling-big-ideas-in-a-bruising-workplace.html)

  - [I Had a Baby and Cancer When I Worked at Amazon. This Is My Story](https://medium.com/@jcheiffetz/i-had-a-baby-and-cancer-when-i-worked-at-amazon-this-is-my-story-9eba5eef2976#.jryxto5jz)

  - [Full memo: Jeff Bezos responds to brutal NYT story, says it doesn't represent the Amazon he leads](http://www.geekwire.com/2015/full-memo-jeff-bezos-responds-to-cutting-nyt-expose-says-tolerance-for-lack-of-empathy-needs-to-be-zero/)

  - [Amazon is piloting teams with a 30-hour workweek](https://www.washingtonpost.com/news/the-switch/wp/2016/08/26/amazon-is-piloting-teams-with-a-30-hour-work-week/)

  These articles discuss [burnout]:

  - [The Reality of Developer Burnout](https://www.kennethreitz.org/essays/the-reality-of-developer-burnout)

  - [How to Recognize Burnout Before You’re Burned Out](https://www.nytimes.com/2017/09/05/smarter-living/workplace-burnout-symptoms.html?sl_l=1&sl_rec=editorial&contentCollection=smarter-living&mData=articles%255B%255D%3Dhttps%253A%252F%252Fwww.nytimes.com%252F2017%252F09%252F05%252Fsmarter-living%252Fworkplace-burnout-symptoms.html%253Fsl_l%253D1%2526sl_rec%253Deditorial%26articles%255B%255D%3Dhttps%253A%252F%252Fwww.nytimes.com%252F2017%252F08%252F21%252Fwell%252Flive%252Ffat-bias-starts-early-and-takes-a-serious-toll.html%253Fsl_rec%253Deditorial&hp&action=click&pgtype=Homepage&clickSource=story-heading&module=smarterLiving-promo-region&region=smarterLiving-promo-region&WT.nav=smarterLiving-promo-region)

  - [Burnout and Recovery at a Tech Internship](https://code.likeagirl.io/burnout-and-recovery-at-a-tech-internship-88d59aded71d)

  These articles discuss the importance (or lack of importance) of balance:

  - ["Eat, sleep, code, repeat" is such bullshit](https://m.signalvnoise.com/eat-sleep-code-repeat-is-such-bullshit-c2a4d9beaaf5#.owywlhd0o)

  - [The Research Is Clear: Long Hours Backfire for People and for Companies](https://hbr.org/2015/08/the-research-is-clear-long-hours-backfire-for-people-and-for-companies)

  - [Maybe We All Need a Little Less Balance](http://archive.is/klBMh#selection-1958.0-1958.1)

  [Amazon]: https://www.amazon.com

  ### Bonus

  Maybe relevant.  Maybe not.

  - [On Parenthood](http://blog.codinghorror.com/on-parenthood/)

  ## Questions

  Please write a response to one of the following questions:

  1. From the readings, what is the controversy surrounding the [H-1B Visa]
  program?  What are the arguments for and against the expansion of the
  program?  After examining the topic, where do you stand on the issues
  surrounding the program?

      - If you are in favor of expanding the use of [H-1B Visa] guest workers,
        explain why it is beneficial for the United States.  How would you
        respond to concerns about lowering of wages or loss of jobs for US
        citizens?  Shouldn't the country prioritize Americans first?

      - If you are against the use of [H-1B Visa] guest workers, explain why it
        is is not necessary or potentially harmful to the United States.  How
        would you respond to the idea that America is a [nation of immigrants]
        and that these guest workers are an effective means of tackling the
        problem of a tech talent shortage?  What do you say to some of your
        classmates who will be applying to these programs?

      - In either case, discuss whether or not you are concerned with
        competition due to foreign workers or possibly outsourcing.  Should the
        US curtail programs like the [H-1B Visa] or rescind [DACA] in order to
        prioritize Americans?  Or should it live up to its image as the [land of opportunity]?

          If you are non-US citizen, discuss how these issues impact you and
          your future plans as it relates to residency and employment in
          America.

  [H-1B Visa]:              https://www.uscis.gov/eir/visa-guide/h-1b-specialty-occupation/h-1b-visa
  [nation of immigrants]:   https://en.wikipedia.org/wiki/A_Nation_of_Immigrants
  [land of opportunity]:    https://en.wikipedia.org/wiki/American_Dream

  2. From the readings and from your experience, can men and women [have it
  all]?  That is, can parents have successful and fulfilling careers while also
  raising a family and meeting other non-work related goals?

      - What does it mean to [have it all] to you?  What examples from real
        life do you draw from in order to define what having a balance is?

      - Have you ever dealt with [burnout] or guilt over missing out on some
        portion of your life?  If so, describe how you dealt with this
        situation and what helped you overcome these difficulties.

      - What can companies do to support their workers to find this balance and
        are they ethically obliged to do so?  Was the opportunity for balance
        something that factored into your choice of career or job opportunity?
        Why or why not?

      - Finally, is this balance important to you and if so, how do you hope to
        maintain it?  What life-style changes or activities have you put in
        place to deal with finding a balance in your life and preventing
        [burnout]?

  [have it all]:    http://maxschireson.com/2014/08/05/1137/
  [startups]:       http://www.nytimes.com/2015/10/11/magazine/silicon-valleys-most-elusive-beast.html?_r=0
  [burnout]:        http://www.bbc.com/capital/story/20161116-stress-is-good-for-you-until-it-isnt?ocid=global_capital_rss

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.
