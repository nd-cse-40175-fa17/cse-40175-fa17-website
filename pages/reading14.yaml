title:      "Reading 14: Computer Science Education"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week are focused on Computer Science Education:

  ### Computer Science 4 All

  These articles discuss the political push for Computer Science 4 All:

  - [Ivanka Trump: Computer science education a new “priority”](https://arstechnica.com/tech-policy/2017/09/ivanka-trump-computer-science-education-a-new-priority/)

  - [President Donald Trump and his daughter Ivanka are unveiling a new federal computer science initiative with major tech backers](https://www.recode.net/2017/9/25/16276904/president-donald-trump-ivanka-tech-stem-computer-science-coding-education-amazon-google)

  - [A Plan to Teach Every Child Computer Science](http://www.theatlantic.com/education/archive/2016/10/a-plan-to-teach-every-child-computer-science/504587/)

  - [What happened to Obama’s $4 billion in computer science funding?](http://www.theverge.com/2016/12/11/13849766/obama-computer-science-for-all-funding)

  These articles discuss how some cities and states are considering implementing CS 4 All:

  - [De Blasio to Announce 10-Year Deadline to Offer Computer Science to All Students](http://www.nytimes.com/2015/09/16/nyregion/de-blasio-to-announce-10-year-deadline-to-offer-computer-science-to-all-students.html?_r=1)

  - [Computer science is space race of today](http://www.kansas.com/opinion/opn-columns-blogs/article86754587.html)

  - [CPS to roll out computer science requirement](http://www.chicagotribune.com/bluesky/originals/ct-computer-science-graduation-cps-bsi-20160225-story.html)

  - [French, Spanish, German ... Java? Making Coding Count As A Foreign Language ](http://www.npr.org/sections/ed/2016/03/01/468695376/french-spanish-german-java-making-coding-count-as-a-foreign-language)

  These articles discuss what should go into CS 4 All:

  - [Learning to Think Like a Computer](https://www.nytimes.com/2017/04/04/education/edlife/teaching-students-computer-code.html?ref=oembed&_r=0)

  - [Is Coding the New Literacy?](http://www.motherjones.com/media/2014/06/computer-science-programming-code-diversity-sexism-education)

  - [Should Computer Education Cover More Than Just Coding?](http://www.npr.org/sections/ed/2016/02/15/465467155/should-computer-education-cover-more-than-just-coding)

  - [Why should we teach programming (Hint: It’s not to learn problem-solving)](https://computinged.wordpress.com/2017/10/18/why-should-we-teach-programming-hint-its-not-to-learn-problem-solving/)

  These articles push back on the CS 4 All movement:

  - [Computing for all #2: Can we get off the pendulum?](http://hub.mspnet.org/index.cfm/29090)

  - [The folly of teaching computer science to high school kids](http://nypost.com/2015/09/20/the-folly-of-teaching-computer-science-to-high-school-kids/)

  - [Jason Bradbury: Coding lessons in schools are a waste of time](http://www.trustedreviews.com/news/jason-bradbury-coding-lessons-in-schools-are-a-waste-of-time)

  - [Please don’t learn to code](https://techcrunch.com/2016/05/10/please-dont-learn-to-code/)

  These articles discuss the obstacles to implementing CS 4 All:

  - [How Much CS Ed Will $1.3B Buy You? Not Enough to Reach Every U.S. School, nor Every Child](https://cacm.acm.org/blogs/blog-cacm/221427-how-much-cs-ed-will-1-3b-buy-you-not-enough-to-reach-every-u-s-school-nor-every-child/fulltext)

  - [Is the U.S. Education System Ready for CS for All?](https://cacm.acm.org/magazines/2017/8/219599-is-the-u-s-education-system-ready-for-cs-for-all/fulltext)

  - [The difficult realities of implementing #CSforAll](http://www.educationdive.com/news/the-difficult-realities-of-implementing-csforall/418127/)

  - [More Teachers, Fewer 3D Printers: How to Improve K–12 Computer Science Education](https://spectrum.ieee.org/tech-talk/at-work/education/what-500-million-could-mean-for-k12-computer-science-education)

  These articles discuss how companies are helping support this movement:

  - [Google announces $1 billion job training and education program ](https://www.axios.com/google-announces-1-billion-job-training-and-education-program-2495966011.html)

  - [Apple launches coding camps for kids in its retail stores](https://techcrunch.com/2016/06/21/apple-launches-coding-camps-for-kids-in-its-retail-stores/)

  - [GE CEO Jeff Immelt says all new hires will learn to code](http://money.cnn.com/2016/08/04/technology/general-electric-coding-jeff-immelt/index.html)

  - [Top business leaders, 27 governors, urge Congress to boost computer science education](https://www.washingtonpost.com/local/education/top-business-leaders-27-governors-urge-congress-to-boost-computer-science-education/2016/04/25/f161cbde-0ae7-11e6-bfa1-4efa856caf2a_story.html)

  These articles discuss whether or not everyone can learn to program:

  - [The Camel Has Two Humps](http://www.eis.mdx.ac.uk/research/PhDArea/saeed/paper1.pdf)

  - [Separating Programming Sheep from Non-Programming Goats](http://blog.codinghorror.com/separating-programming-sheep-from-non-programming-goats/)

  - [Please Don't Learn to Code](http://blog.codinghorror.com/please-dont-learn-to-code/)

  - [Anyone Can Learn Programming: Teaching > Genetics](http://cacm.acm.org/blogs/blog-cacm/179347-anyone-can-learn-programming-teaching-genetics/fulltext)

  ### Computer Science Education

  These articles focus on what should go in a Computer Science Education:

  - [Computer Science 2013: Curriculum Guidelines for Undergraduate Programs in Computer Science](https://www.acm.org/binaries/content/assets/education/cs2013_web_final.pdf)

  - [Criteria for Accrediting Computing Programs, 2016-2017](http://www.abet.org/accreditation/accreditation-criteria/criteria-for-accrediting-computing-programs-2016-2017/)

  - [What every computer science major should know](http://matt.might.net/articles/what-cs-majors-should-know/)

  - [The Perils of Java Schools](http://www.joelonsoftware.com/articles/ThePerilsofJavaSchools.html)

  - [Why I'm Not Looking to Hire Computer-Science Majors](http://www.wsj.com/articles/why-im-not-looking-to-hire-computer-science-majors-1440804753)

  These articles discuss whether or not college is needed for Computer Science:

  - [More Universities Should Shut Down Their Computer Science Programs](http://blog.jeffreymcmanus.com/1924/more-universities-should-shut-down-their-computer-science-programs/)

  - [Turn On, Code In, Drop Out: Tech Programmers Don't Need College Diplomas](https://www.good.is/articles/turn-on-code-in-drop-out)

  - [The limited value of a computer science education](http://nathanmarz.com/blog/the-limited-value-of-a-computer-science-education.html)

  - [Warning: A computer science degree may be a waste of your time and money](http://mashable.com/2014/12/16/warning-college-may-be-a-waste-of-your-time-and-money/#Zv8KzgWM1mqw)

  - [Math Nerds vs. Code Monkeys: Should Computer Science Classes Be More Practical?](http://blog.smartbear.com/careers/math-nerds-vs-code-monkeys-should-computer-science-classes-be-more-practical/)

  - [Dropouts Need Not Apply: Silicon Valley Asks Mostly for Developers With Degrees](http://blogs.wsj.com/economics/2016/03/30/dropouts-need-not-apply-silicon-valley-asks-mostly-for-developers-with-degrees/?mod=e2fb)

  These articles discuss whether or not bootcamps are viable alternatives to a
  traditional university education:

  - [Bootcamps vs. College](http://blog.triplebyte.com/bootcamps-vs-college)

  - [Comparing Salaries for Coding Bootcamps vs. Computer Science Degrees](https://blog.bloc.io/comparing-salaries-for-coding-bootcamps-vs-computer-science-degrees/)

  - [I spent 3 months applying to jobs after a coding bootcamp. Here’s what I learned.](https://medium.freecodecamp.com/5-key-learnings-from-the-post-bootcamp-job-search-9a07468d2331#.dlkowsjat)

  - [So that whole coding bootcamp thing is a scam, right?](https://medium.freecodecamp.com/so-that-whole-coding-bootcamp-thing-is-a-scam-right-6fddf14087d4)

  - [The Kentucky Startup That Is Teaching Coal Miners to Code](https://spectrum.ieee.org/energywise/energy/fossil-fuels/the-kentucky-startup-that-is-teaching-coal-miners-to-code)

  - [Why More Tech Companies Are Hiring People Without Degrees](https://www.fastcompany.com/3069259/why-more-tech-companies-are-hiring-people-without-degrees)

  These articles discuss the boom in CS enrollment:

  - [Generation CS](http://cra.org/data/generation-cs/)

  - [Generation CS: When Undergraduates Realized They Needed Computing](https://cacm.acm.org/blogs/blog-cacm/215245-generation-cs-when-undergraduates-realized-they-needed-computing/fulltext)

  - [University computer science finally surpasses its 2003 peak!](https://medium.com/anybody-can-learn/university-computer-science-finally-surpasses-its-2003-peak-ecefa4c8d77d)

  - [A History of Capacity Challenges in Computer Science](http://cs.stanford.edu/people/eroberts/CSCapacity/)

  - [Analysis: The exploding demand for computer science education, and why America needs to keep up](http://www.geekwire.com/2014/analysis-examining-computer-science-education-explosion/)

  ## Questions

  Please write a response to one of the following questions:

  1. After reading the articles, do you believe that coding is the new
  literacy? Should everyone be exposed or required to take a computer science
  or coding class?

      - What are the arguments for and against introducing everyone to
        computing or programming?  What challenges will schools face as this
        CS4All push moves forward?

      - How should computer science fit into a typical K-12 curriculum?  Is it
        an elective or a requirement?  Does it replace existing subjects or is
        it an addition?  What exactly should be taught in this CS4All
        curriculum?  Is this computational thinking?  programming? logic?
        computer literacy?

      - Can anyone learn to program?  Should everyone learn to program?
        Explain why or why not to both.

  2. Review the [Computer Science and Engineering] course [catalog], and
  compare it to the [Computer Science 2013: Curriculum Guidelines] and [ABET
  Criteria].

      - How does [Notre Dame]'s computer science curriculum match up with the
        the [ACM] guidelines and the [ABET] criteria?  Does it mostly meet
        these learning goals or does it fall short?

      - What do you think of the [ACM] or [ABET] guidelines for a computer
        science program?  What areas or topics need more exposure?  Conversely,
        what areas or topics require less coverage?

      - What do you think of coding **bootcamps**?  Would it have been better
        if you just went to a **bootcamp** out of high school or perhaps gotten
        a degree in another major and then done a **bootcamp** program?  Do
        **bootcamps** replace a college degree?

      - Do you need to go to college to be a good computer scientist, computer
        engineer, software developer, or programmer?  Do you feel [Notre Dame]
        has prepared you adequately for your future career?  Do you know
        everything you should know (or want to know)?  Explain.

  [Computer Science 2013: Curriculum Guidelines]:  http://www.acm.org/education/CS2013-final-report.pdf
  [Computer Science and Engineering]: http://cse.nd.edu/
  [catalog]: https://ssb.cc.nd.edu/pls/BNRPROD/bwckctlg.p_disp_dyn_ctlg
  [ABET Criteria]: http://www.abet.org/accreditation/accreditation-criteria/criteria-for-accrediting-computing-programs-2016-2017/
  [Notre Dame]: http://www.nd.edu
  [ACM]: http://www.acm.org/
  [ABET]: http://www.abet.org

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.
